# Team Members

* Josh Cullings
* Brendan Blanchard
* Yue Hou
* member 4

# Basic Normal Form (BCNF)

**Question 1**
How to Convert to Basic Normal Form (BCNF)

**Question 2**
To concisely explain a process to convert to BCNF from a relationship schema.

**Question 3**
By reading this, the reader will understand in simple steps how to write and read data represented in BCNF.

# Statements in SQlite

**Question 1**
SQlite Statements: A Crash Course

**Question 2**
To convey how to read and write statements in SQlite.

**Question 3**
By reading this tutorial, the reader will be able to intereperet SQlite statements, through the understanding of the components SQl statements, on their own.

# ER Diagrams Navigation and Reading

**Question 1**
A Quick Guide to the Navigation of ER Diagrams

**Question 2**
To facilitate a better understanding of how to read and understand information represented by an ER diagram.

**Question 3**
They would be able to glean information from the logical representation databases in ER diagrams.